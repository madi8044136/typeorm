import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productsRepository = AppDataSource.getRepository(Product)
    // const product = new Product()
    // product.name = "Espresso";
    // product.price = 50;
    // await productsRepository.save(product)
    // console.log("Saved a new product with id: " + product.id)

    console.log("Loading products from the database...")
    const products = await productsRepository.find()
    console.log("Loaded products: ", products)

    const updateProduct = await productsRepository.findOneBy({id: 3})
    console.log(updateProduct)
    updateProduct.price = 50
    await productsRepository.save(updateProduct)

}).catch(error => console.log(error))
